import { useEffect, useState, useContext } from "react";
import axios from "axios";
import QuizList from "./components/QuizList";
import LoadingSpinner from "./components/LoadingSpinner";
import Modal from "./components/Modal";
import { useNavigate } from "react-router-dom";
import { SocketContext } from "./providers/SocketProvider";

export default function App() {
  const [quizzes, setQuizzes] = useState([]);
  const [loading, setLoading] = useState(true);
  const [participants, setParticipants] = useState([]);
  const [isUsernameModalOpen, setUsernameModalOpen] = useState(true);

  const navigate = useNavigate();
  const socket = useContext(SocketContext);

  useEffect(() => {
    socket.on("updateParticipants", (updatedParticipants) => {
      setParticipants(updatedParticipants);
    });

    socket.on("quizStarted", ({ params, questions, category }) => {
      navigate(`/quiz/${params.quizId}/${params.difficulty}`, {
        state: { questions, category },
      });
    });
  }, [socket, navigate]);

  const joinLobby = (username) => {
    if (username.trim() !== "") {
      socket.emit("joinLobby", username);
      setUsernameModalOpen(false);
    }
  };

  const startQuiz = (params) => {
    socket.emit("startQuiz", params);
  };

  useEffect(() => {
    const fetchQuizzes = async () => {
      try {
        const response = await axios.get(
          `${import.meta.env.VITE_API_DOMAIN}/api/quiz`
        );
        setQuizzes(response.data);
      } catch (error) {
        console.error("Error fetching quizzes:", error.message);
      } finally {
        setLoading(false);
      }
    };
    fetchQuizzes();
  }, [loading]);

  return (
    <main className="flex flex-col items-center justify-center min-h-screen text-white">
      <header className="text-4xl color-indigo-600 font-bold mb-8 animate__animated animate__fadeIn">
        Brain Brawl
      </header>
      <Modal isOpen={isUsernameModalOpen} onConfirm={joinLobby} />
      {loading ? (
        <LoadingSpinner />
      ) : (
        <div className="flex flex-col items-center mt-8">
          <div className="flex flex-col items-center justify-center mb-6">
            <h2 className="text-2xl font-bold mb-4">Participants:</h2>
            <div className="flex flex-wrap gap-4">
              {participants.map((participant, index) => (
                <div
                  key={index}
                  className="bg-gray-600 p-4 rounded-full shadow-md text-lg overflow-hidden flex items-center justify-center"
                  style={{ width: `${participant.username.length * 1.5}ch` }}
                >
                  {participant.username}
                </div>
              ))}
            </div>
          </div>
          <QuizList quizzes={quizzes} onStartQuiz={startQuiz} />
        </div>
      )}
    </main>
  );
}

import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";
import QuestionList from "../components/QuestionList";
import LoadingSpinner from "../components/LoadingSpinner";
import "animate.css";

export default function Quiz() {
  const [category, setCategory] = useState("");
  const [questions, setQuestions] = useState([]);
  const [loading, setLoading] = useState(true);
  const location = useLocation();

  useEffect(() => {
    const fetchQuestions = async () => {
      try {
        const fetchedQuestions = location.state?.questions || [];
        const fetchedCategoryName = location.state?.category || "";

        if (fetchedQuestions.length > 0) {
          setQuestions(fetchedQuestions);
          setCategory(fetchedCategoryName);
          setLoading(false);
        }
      } catch (error) {
        console.error("Error fetching quizzes:", error.message);
        setLoading(false);
      }
    };
    fetchQuestions();
  }, [location.state]);

  return (
    <>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <>
          <h2 className="text-4xl font-extrabold text-center mb-6 animate__animated animate__fadeIn">
            {category}
          </h2>
          <QuestionList questions={questions} />
        </>
      )}
    </>
  );
}

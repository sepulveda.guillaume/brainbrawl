import DifficultyButtons from "./DifficultyButtons";
import { decode } from "html-entities";

export default function QuizList({ quizzes, onStartQuiz }) {
  const reorganizeQuizzes = quizzes
    .reduce((acc, quiz) => {
      const categoryIndex = acc.findIndex(
        (item) => item.category === quiz.category
      );
      if (categoryIndex === -1) {
        acc.push({ id: quiz._id, category: quiz.category, quizzes: [quiz] });
      } else {
        acc[categoryIndex].quizzes.push(quiz);
      }
      return acc;
    }, [])
    .sort((a, b) => {
      if (a.category === "Random") return -1;
      if (b.category === "Random") return 1;
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    });

  return (
    <div className="max-w-screen-lg mx-auto">
      {reorganizeQuizzes.map((quiz) => (
        <div
          key={quiz.id}
          className="bg-indigo-600 shadow-md p-6 rounded-md mb-8"
        >
          <h2 className="text-2xl font-bold mb-6 capitalize">
            {decode(quiz.category)}
          </h2>
          <div className="flex flex-wrap justify-center gap-4">
            {quiz.quizzes.map((quiz) => (
              <DifficultyButtons
                key={quiz._id}
                quizId={quiz._id}
                difficulty={quiz.difficulty}
                onStartQuiz={onStartQuiz}
              />
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}

import { useState, useEffect, useContext } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { SocketContext } from "../providers/SocketProvider";
import { decode } from "html-entities";

export default function QuestionList({ questions }) {
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [isCorrect, setIsCorrect] = useState(null);
  const [correctAnswer, setCorrectAnswer] = useState(null);
  const [score, setScore] = useState(0);
  const [remainingQuestions, setRemainingQuestions] = useState(
    questions.length
  );
  const [endQuiz, setEndQuiz] = useState(false);
  const [countdown, setCountdown] = useState(3);
  const [newQuestionCountdown, setNewQuestionCountdown] = useState(2);
  const [participants, setParticipants] = useState([]);
  const [loading, setLoading] = useState(true);

  const socket = useContext(SocketContext);

  useEffect(() => {
    socket.on("quizEnded", (updateParticipants) => {
      setParticipants(updateParticipants);
    });
  }, [socket]);

  useEffect(() => {
    let timer;

    if (countdown > 0 && remainingQuestions > 0) {
      timer = setInterval(() => {
        setCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
    }
    if (countdown === 0 && remainingQuestions > 0 && loading) {
      if (!selectedAnswer) {
        checkAnswer(" ");
      } else {
        checkAnswer(selectedAnswer);
      }
      clearInterval(timer);
    }

    return () => {
      clearInterval(timer);
    };
  }, [countdown, remainingQuestions, selectedAnswer, loading]);

  useEffect(() => {
    let newQuestionTimer;

    if (newQuestionCountdown > 0 && remainingQuestions > 0 && countdown === 0) {
      newQuestionTimer = setInterval(() => {
        setNewQuestionCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
    }
    if (
      newQuestionCountdown === 0 &&
      remainingQuestions > 0 &&
      countdown === 0
    ) {
      handleNextQuestion();
      clearInterval(newQuestionTimer);
    }

    return () => {
      clearInterval(newQuestionTimer);
    };
  }, [newQuestionCountdown, remainingQuestions, countdown]);

  const handleAnswerSelection = (answer) => {
    setSelectedAnswer(answer);
  };

  const checkAnswer = async (answer) => {
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_API_DOMAIN}/api/quiz/checkanswer`,
        {
          _id: questions[currentQuestionIndex]._id,
          answer: answer,
        }
      );

      setIsCorrect(response.data.isCorrect);
      setCorrectAnswer(response.data.correctAnswer);
      updateScore(response.data.isCorrect);
    } catch (error) {
      console.error("Error fetching quizzes:", error.message);
    } finally {
      setLoading(false);
    }
  };

  const handleNextQuestion = () => {
    setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    setSelectedAnswer(null);
    setIsCorrect(null);
    setCountdown(3);
    setNewQuestionCountdown(2);
    setLoading(true);
  };

  const updateScore = (isCorrect) => {
    setScore((prevScore) => (isCorrect ? prevScore + 1 : prevScore));
    setRemainingQuestions(questions.length - currentQuestionIndex - 1);
  };

  const handleEndQuiz = () => {
    setEndQuiz(true);
    socket.emit("endQuiz", score);
  };

  const handleDisconnect = () => {
    socket.emit("disconnect");
  };

  return (
    <div className="animate__animated animate__fadeIn">
      {countdown >= 0 && !endQuiz && (
        <div className="flex flex-col items-center my-3">
          <div
            className={`text-3xl font-semibold h-16 w-16 rounded-full bg-white flex items-center justify-center font-mono ${
              countdown < 3 ? " text-red-500" : "text-gray-700"
            }`}
          >
            {countdown}
          </div>
        </div>
      )}
      {questions.length > 0 &&
        currentQuestionIndex < questions.length &&
        !endQuiz && (
          <div className="text-center">
            <h3 className="text-xl font-semibold mb-4">
              Question {currentQuestionIndex + 1}
            </h3>
            <p className="mb-6">
              {decode(questions[currentQuestionIndex].questionText)}
            </p>
            <ul className="grid grid-cols-1 sm:grid-cols-2 gap-4">
              {questions[currentQuestionIndex].allAnswers.map((answer) => (
                <li
                  key={answer}
                  onClick={() => handleAnswerSelection(answer)}
                  className={`cursor-pointer p-4 text-white font-semibold rounded ${
                    selectedAnswer === answer &&
                    (countdown > 0 || (countdown === 0 && loading))
                      ? "bg-indigo-600 text-white"
                      : isCorrect &&
                        countdown === 0 &&
                        selectedAnswer === answer &&
                        !loading
                      ? "bg-green-500 text-white"
                      : !isCorrect &&
                        countdown === 0 &&
                        !loading &&
                        (answer === correctAnswer || selectedAnswer === " ")
                      ? "bg-red-500 text-white"
                      : "bg-gray-500 text-white"
                  }`}
                >
                  {answer}
                </li>
              ))}
            </ul>
            {countdown === 0 && !loading && (
              <div className="mt-6">
                <p className="text-lg">
                  Votre réponse est{" "}
                  <span
                    className={`font-semibold ${
                      isCorrect ? "text-green-500" : "text-red-500"
                    }`}
                  >
                    {isCorrect ? "correcte" : "incorrecte"}
                  </span>
                  {!isCorrect && (
                    <span className="block mt-2 text-green-500 font-bold">
                      Réponse correcte: {correctAnswer}
                    </span>
                  )}
                </p>
                {remainingQuestions > 0 ? (
                  <>
                    <div className="my-4 p-2">
                      Prochaine question dans{" "}
                      <div className="flex flex-col items-center my-3">
                        <div
                          className={`text-2xl font-semibold text-gray-700 h-10 w-10 rounded-full bg-white flex items-center justify-center font-mono`}
                        >
                          {newQuestionCountdown}
                        </div>
                      </div>
                    </div>
                    <div className="mt-4 font-bold">
                      Score: {score} /{" "}
                      {currentQuestionIndex + 1 === 1
                        ? "1 bonne réponse"
                        : `${currentQuestionIndex + 1} bonnes réponses`}
                    </div>
                    <div className="mt-4">
                      {remainingQuestions === 1
                        ? "1 question restante"
                        : `${remainingQuestions} questions restantes`}
                    </div>
                  </>
                ) : (
                  <button
                    className="mt-4 bg-indigo-600 text-white mx-3 p-4 rounded-full hover:bg-indigo-500 transition-all duration-300"
                    onClick={handleEndQuiz}
                  >
                    Voir mon score
                  </button>
                )}
              </div>
            )}
          </div>
        )}
      {endQuiz && (
        <div className="text-center">
          <h2 className="text-2xl font-extrabold mb-8">Scores:</h2>
          <ul className="list-none pb-8">
            {participants
              .sort((a, b) => b.score - a.score)
              .map((participant) => (
                <li key={participant.id} className={`text-lg m-2`}>
                  {participant.username} : {participant.score}
                </li>
              ))}
          </ul>
          <Link
            to="/"
            className="bg-indigo-600 text-white mx-3 p-4 rounded-full hover:bg-indigo-500 transition-all duration-300"
            onClick={handleDisconnect}
          >
            Retour à l&apos;accueil
          </Link>
        </div>
      )}
    </div>
  );
}

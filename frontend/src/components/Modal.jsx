import { useState } from "react";

export default function Modal({ isOpen, onConfirm }) {
  const [username, setUsername] = useState("");

  const handleConfirm = () => {
    onConfirm(username);
  };

  return (
    <div
      className={`fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 flex items-center justify-center ${
        isOpen ? "visible" : "hidden"
      } z-50`}
    >
      <div className="bg-white p-8 rounded shadow-md relative z-50 w-96">
        <h2 className="text-3xl font-bold mb-4 text-gray-700">Votre nom</h2>
        <input
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          className="w-full p-3 mb-6 border border-gray-300 rounded focus:outline-none focus:border-indigo-600"
          placeholder="Your Name"
        />
        <div className="flex justify-center">
          <button
            onClick={handleConfirm}
            className="bg-indigo-600 text-white px-4 py-2 rounded-full hover:bg-indigo-500 transition-all duration-300"
          >
            Confirmer
          </button>
        </div>
      </div>
    </div>
  );
}

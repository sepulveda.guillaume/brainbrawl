import { Link } from "react-router-dom";

export default function DifficultyButtons({ quizId, difficulty, onStartQuiz }) {
  // Function to map difficulty levels to stars
  const difficultyToStars = (difficulty) => {
    switch (difficulty) {
      case "Easy":
        return "⭐";
      case "Medium":
        return "⭐⭐";
      case "Hard":
        return "⭐⭐⭐";
      default:
        return difficulty; // Return original difficulty if not recognized
    }
  };

  const stars = difficultyToStars(difficulty);

  const handleStartQuiz = (params) => {
    onStartQuiz(params);
  };

  return (
    <Link
      key={quizId}
      to={`quiz/${quizId}/${difficulty}`}
      onClick={() => handleStartQuiz({ quizId, difficulty })}
      className="mx-3 mb-3 p-4 text-white border-solid border-2 border-white rounded-md transition-all duration-300 hover:bg-indigo-500 animate__animated animate__fadeIn"
    >
      <div>{stars}</div>
    </Link>
  );
}

const mongoose = require("mongoose");
const Question = require("./Question");

const quizSchema = new mongoose.Schema({
  category: {
    type: String,
    required: true,
  },
  difficulty: {
    type: String,
    required: true,
  },
  questions: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Question",
    },
  ],
});
const Quiz = mongoose.model("Quiz", quizSchema);

module.exports = Quiz;

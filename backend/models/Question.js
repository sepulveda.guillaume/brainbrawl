const mongoose = require("mongoose");

const questionSchema = new mongoose.Schema({
  questionText: {
    type: String,
    required: true,
  },
  correctAnswer: {
    type: String,
    required: true,
  },
  incorrectAnswers: [
    {
      type: String,
      required: true,
    },
  ],
  category: {
    type: String,
    required: true,
  },
  difficulty: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: false,
  },
  anecdote: {
    type: String,
    required: false,
  },
});

const Question = mongoose.model("Question", questionSchema);

module.exports = Question;

const Quiz = require("../models/Quiz");
const Question = require("../models/Question");

// const getAllQuizzes = async (req, res) => {
//   try {
//     const quizzes = await Quiz.find().populate("questions");

//     // return for each questions only the _id and difficulty
//     const modifiedQuizzes = quizzes.map((quiz) => {
//       const modifiedQuestions = quiz.questions.map(({ _id, difficulty }) => ({
//         _id,
//         difficulty,
//       }));
//       return {
//         ...quiz.toObject(),
//         questions: modifiedQuestions,
//       };
//     });
//     res.json(modifiedQuizzes);
//   } catch (error) {
//     console.error("Error in getAllQuizzes:", error);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// };

const getAllQuizzes = async (req, res) => {
    try {
      const quizzes = await Quiz.find();
      res.json(quizzes);
    } catch (error) {
      console.error("Error in getAllQuizzes:", error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  };

const getQuizById = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id).populate("questions");

    if (req.params.difficulty) {
      quiz.questions = quiz.questions.filter(
        (question) => question.difficulty === req.params.difficulty
      );
    }

    if (!quiz) {
      return res.status(404).json({ error: "Quiz not found" });
    }

    // Modify the quizzes to include a new key with all answers for each question
    const questions = await Question.find({ _id: { $in: quiz.questions } });

    const modifiedQuestions = questions.map((question) => ({
      ...question.toObject(),
      allAnswers: [...question.incorrectAnswers, question.correctAnswer].sort(
        () => Math.random() - 0.5
      ),
    }));

    // remove correctAnswer and incorrectAnswers from the response
    modifiedQuestions.forEach((question) => {
      delete question.correctAnswer;
      delete question.incorrectAnswers;
    });

    res.json({
      ...quiz.toObject(),
      questions: modifiedQuestions,
    });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const checkAnswer = async (req, res) => {
  try {
    const question = await Question.findById(req.body._id);
    if (!question) {
      return res.status(404).json({ error: "Question not found" });
    }

    const isCorrect = req.body.answer === question.correctAnswer;
    res.json({ isCorrect, correctAnswer: question.correctAnswer });
  } catch (error) {
    console.error("Error in checkAnswer:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const postQuiz = async (req, res) => {
  try {
    const quiz = await Quiz.create(req.body).populate("questions");
    res.status(201).json(quiz);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const putQuiz = async (req, res) => {
  try {
    const quiz = await Quiz.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    }).populate("questions");
    if (!quiz) {
      return res.status(404).json({ error: "Quiz not found" });
    }
    res.json(quiz);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const deleteQuiz = async (req, res) => {
  try {
    const quiz = await Quiz.findByIdAndDelete(req.params.id).populate(
      "questions"
    );
    if (!quiz) {
      return res.status(404).json({ error: "Quiz not found" });
    }
    res.json({ message: "Quiz deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getAllQuizzes,
  getQuizById,
  checkAnswer,
  postQuiz,
  putQuiz,
  deleteQuiz,
};

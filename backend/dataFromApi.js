const axios = require("axios");
const mongoose = require("mongoose");
const QuestionModel = require("./models/Question");
const QuizModel = require("./models/Quiz");

require("dotenv").config();

async function fetchDataFromApi() {
  try {
    const response = await axios.get(
      "https://quizzapi.jomoreschi.fr/api/v1/quiz"
    );
    return response.data;
  } catch (error) {
    console.error("Error fetching data from API:", error.message);
    throw error;
  }
}

async function connectToDatabase() {
  try {
    await mongoose.connect(process.env.MANGOOSE_CONNECTION);
    console.log("Connection to MongoDB successful");
  } catch (error) {
    console.error("Connection to MongoDB failed:", error.message);
    throw error;
  }
}

async function organizeQuizzesByCategory(data) {
  try {
    const organizedQuizzes = {};
    const allQuestions = [];

    for (const quizData of data.quizzes) {
      const question = new QuestionModel({
        questionText: quizData.question,
        correctAnswer: quizData.answer,
        incorrectAnswers: quizData.badAnswers,
        category: quizData.category
          .split("_")
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(" "),
        difficulty:
          quizData.difficulty[0].toUpperCase() + quizData.difficulty.slice(1),
      });

      await question.save();
      allQuestions.push(question._id);

      if (!organizedQuizzes[quizData.category]) {
        organizedQuizzes[quizData.category] = {
          category: quizData.category,
          questions: [],
        };
      }
      organizedQuizzes[quizData.category].questions.push(question._id);
    }

    organizedQuizzes.all = {
      category: "Aléatoire",
      questions: allQuestions,
    };

    return organizedQuizzes;
  } catch (error) {
    console.error("Error organizing quizzes by category:", error.message);
    throw error;
  }
}

async function importData(organizedQuizzes) {
  try {
    for (const [category, quizData] of Object.entries(organizedQuizzes)) {
      const quiz = new QuizModel({
        category: category
          .split("_")
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(" "),
        questions: quizData.questions,
      });

      await quiz.save();
    }

    console.log("Data import successful!");
  } catch (error) {
    console.error("Error importing data:", error.message);
  }
}

async function run() {
  try {
    await connectToDatabase();
    const apiData = await fetchDataFromApi();
    const organizedQuizzes = await organizeQuizzesByCategory(apiData);
    await importData(organizedQuizzes);
  } catch (error) {
    console.error("Error in the run function:", error.message);
  } finally {
    mongoose.connection.close();
    console.log("Disconnected from MongoDB");
  }
}

run();

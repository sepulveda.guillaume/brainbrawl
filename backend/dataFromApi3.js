const axios = require("axios");
const mongoose = require("mongoose");
const QuestionModel = require("./models/Question");
const QuizModel = require("./models/Quiz");

require("dotenv").config();

const apiKey = "Q3H7Q2K4RU";

async function fetchDataFromApi(params) {
  try {
    const response = await axios.get(
      `https://api.openquizzdb.org/?key=${apiKey}&anec=1${params}`
    );
    return response.data;
  } catch (error) {
    console.error("Error fetching data from API:", error.message);
    throw error;
  }
}

async function connectToDatabase() {
  try {
    await mongoose.connect(process.env.MANGOOSE_CONNECTION);
    console.log("Connection to MongoDB successful");
  } catch (error) {
    console.error("Connection to MongoDB failed:", error.message);
    throw error;
  }
}

async function organizeQuizzesByCategory(data) {
  try {
    let organizedQuizzes = {};
    const allQuestions = [];

    for (const quizData of data.results) {
      //check if the question is already in the database
      const existingQuestion = await QuestionModel.findOne({
        questionText: quizData.question,
      });
      if (existingQuestion) {
        continue;
      } else {
        // remove from quitData.autres_choix the quizData.reponse_correcte
        quizData.autres_choix = quizData.autres_choix.filter(
          (choice) => choice !== quizData.reponse_correcte
        );

        const question = new QuestionModel({
          questionText: quizData.question,
          correctAnswer: quizData.reponse_correcte,
          incorrectAnswers: quizData.autres_choix,
          category:
            quizData.categorie[0].toUpperCase() + quizData.categorie.slice(1),
          difficulty:
            quizData.difficulte[0].toUpperCase() + quizData.difficulte.slice(1),
          anecdote: quizData.anecdote,
        });

        if (!organizedQuizzes[quizData.categorie]) {
          organizedQuizzes[quizData.categorie] = {
            category:
              quizData.categorie[0].toUpperCase() + quizData.categorie.slice(1),
            difficulty:
              quizData.difficulte[0].toUpperCase() +
              quizData.difficulte.slice(1),
            questions: [],
          };
        }

        await question.save();
        allQuestions.push(question._id);

        if (!organizedQuizzes.Random) {
          if (organizedQuizzes[quizData.difficulte] !== quizData.difficulte) {
            organizedQuizzes.Random = {
              category: "Random",
              difficulty:
                quizData.difficulte[0].toUpperCase() +
                quizData.difficulte.slice(1),
              questions: [],
            };
          }
        }
        organizedQuizzes.Random.questions.push(question._id);

        organizedQuizzes[quizData.categorie].questions.push(question._id);
      }
    }
    return organizedQuizzes;
  } catch (error) {
    console.error("Error organizing quizzes by category:", error.message);
    throw error;
  }
}

async function importData(organizedQuizzes) {
  try {
    if (Object.keys(organizedQuizzes).length === 0) {
      console.log("No new data to import");
      return;
    }
    for (const [category, quizData] of Object.entries(organizedQuizzes)) {
      if (category === "Random") {
        const quiz = await QuizModel.findOne({
          category: "Random",
          difficulty: quizData.difficulty,
        });

        if (quiz) {
          quiz.questions = quiz.questions.concat(quizData.questions);
          await quiz.save();
        } else {
          const quiz = new QuizModel({
            category: "Random",
            difficulty: quizData.difficulty,
            questions: quizData.questions,
          });
          await quiz.save();
        }
      } else {
        const quiz = new QuizModel({
          category: quizData.category,
          difficulty: quizData.difficulty,
          questions: quizData.questions,
        });
        await quiz.save();
      }
    }

    console.log("Data import successful!");
  } catch (error) {
    console.error("Error importing data:", error.message);
  }
}

async function run(params) {
  try {
    const apiData = await fetchDataFromApi(params);
    const organizedQuizzes = await organizeQuizzesByCategory(apiData);
    await importData(organizedQuizzes);
  } catch (error) {
    console.error("Error in the run function:", error.message);
  }
}

// put all value of the previous options in an array
const array = [
  "animaux",
  "archeologie",
  "arts",
  "bd",
  "celebrites",
  "cinema",
  "culture",
  "gastronomie",
  "geographie",
  "histoire",
  "informatique",
  "internet",
  "litterature",
  "loisirs",
  "musique",
  "nature",
  "monde",
  "sciences",
  "sports",
  "television",
  "tourisme",
  "quotidien",
];

const difficulties = ["1", "2", "3"];

async function runSequentially() {
  try {
    await connectToDatabase();

    for (const cat of array) {
      for (const difficulty of difficulties) {
        for (let i = 0; i < 50; i++) {
          console.log(
            `Running for category ${cat}, difficulty ${difficulty}, iteration ${
              i + 1
            }`
          );
          await run(`&category=${cat}&diff=${difficulty}`);
        }
        await delay(5000);
      }
    }
  } catch (error) {
    console.error("Error in runSequentially:", error.message);
  } finally {
    await mongoose.connection.close();
    console.log("Disconnected from MongoDB");
  }
}

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

runSequentially();

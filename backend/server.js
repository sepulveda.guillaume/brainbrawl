const express = require("express");
const app = express();
const port = process.env.PORT || 3001;
const cors = require("cors");
const mongoose = require("mongoose");
const router = require("./routes/index");
const http = require("http");
const { Server } = require("socket.io");
const axios = require("axios");

require("dotenv").config();

const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: true,
    methods: ["GET", "POST"],
  },
});

mongoose
  .connect(process.env.MANGOOSE_CONNECTION)
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

let participants = [];
const questionsCount = 2;

io.on("connection", (socket) => {
  socket.on("joinLobby", (username) => {
    console.log("User joined lobby:", username);
    participants.push({ id: socket.id, username, score: 0 });
    io.emit("updateParticipants", participants);
  });

  socket.on("startQuiz", async (params) => {
    try {
      const response = await axios.get(
        `http://localhost:${port}/api/quiz/${params.quizId}/${params.difficulty}`
      );

      const shuffledQuestions = response.data.questions.sort(
        () => Math.random() - 0.5
      );

      const selectedQuestions = shuffledQuestions.slice(0, questionsCount);

      io.emit("quizStarted", { params, questions: selectedQuestions, category: response.data.category });
    } catch (error) {
      console.error("Error fetching questions:", error.message);
    }
  });

  socket.on("endQuiz", (score) => {
    const participant = participants.find((p) => p.id === socket.id);

    participant ? (participant.score = score) : null;

    io.emit("quizEnded", participants);
  });

  socket.on("disconnect", () => {
    participants = participants.filter((p) => p.id !== socket.id);
    io.emit("updateParticipants", participants);
  });
});

app.use("/", router);

server.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
const router = require("express").Router();
const quizRoutes = require("./quiz.routes");

router.use("/api/quiz", quizRoutes);

module.exports = router;
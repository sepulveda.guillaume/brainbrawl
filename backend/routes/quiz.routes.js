const express = require("express");
const router = express.Router();
const {
  getAllQuizzes,
  getQuizById,
  checkAnswer,
  postQuiz,
  putQuiz,
  deleteQuiz,
} = require("../controllers/quiz.controller");

// GET quizzes
router.get("/", getAllQuizzes);

// GET quiz by id and difficulty
router.get("/:id/:difficulty", getQuizById);

// POST check answer
router.post("/checkanswer", checkAnswer);

// POST new quiz
router.post("/", postQuiz);

// PUT quiz by id
router.put("/:id", putQuiz);

// DELETE quiz by id
router.delete("/:id", deleteQuiz);

module.exports = router;

const axios = require("axios");
const mongoose = require("mongoose");
const QuestionModel = require("./models/Question");
const QuizModel = require("./models/Quiz");

require("dotenv").config();

async function fetchDataFromApi(params) {
  try {
    const response = await axios.get(
      `https://opentdb.com/api.php?amount=50${params}`
    );
    return response.data;
  } catch (error) {
    console.error("Error fetching data from API:", error.message);
    throw error;
  }
}

async function connectToDatabase() {
  try {
    await mongoose.connect(process.env.MANGOOSE_CONNECTION);
    console.log("Connection to MongoDB successful");
  } catch (error) {
    console.error("Connection to MongoDB failed:", error.message);
    throw error;
  }
}

async function organizeQuizzesByCategory(data) {
  try {
    let organizedQuizzes = {};
    const allQuestions = [];

    for (const quizData of data.results) {
      const question = new QuestionModel({
        questionText: quizData.question,
        correctAnswer: quizData.correct_answer,
        incorrectAnswers: quizData.incorrect_answers,
        category: quizData.category,
        difficulty:
          quizData.difficulty[0].toUpperCase() + quizData.difficulty.slice(1),
      });

      if (!organizedQuizzes[quizData.category]) {
        organizedQuizzes[quizData.category] = {
          category: quizData.category,
          difficulty: quizData.difficulty,
          questions: [],
        };
      }

      await question.save();
      allQuestions.push(question._id);

      if (!organizedQuizzes.Random) {
        if (organizedQuizzes[quizData.difficulty] !== quizData.difficulty) {
          organizedQuizzes.Random = {
            category: "Random",
            difficulty: quizData.difficulty,
            questions: [],
          };
        }
      }
      organizedQuizzes.Random.questions.push(question._id);

      organizedQuizzes[quizData.category].questions.push(question._id);
    }
    return organizedQuizzes;
  } catch (error) {
    console.error("Error organizing quizzes by category:", error.message);
    throw error;
  }
}

async function importData(organizedQuizzes) {
  try {
    for (const [category, quizData] of Object.entries(organizedQuizzes)) {
      if (category === "Random") {
        const quiz = await QuizModel.findOne({
          category: "Random",
          difficulty:
            quizData.difficulty[0].toUpperCase() + quizData.difficulty.slice(1),
        });

        if (quiz) {
          quiz.questions = quiz.questions.concat(quizData.questions);
          await quiz.save();
        } else {
          const quiz = new QuizModel({
            category: "Random",
            difficulty:
              quizData.difficulty[0].toUpperCase() +
              quizData.difficulty.slice(1),
            questions: quizData.questions,
          });
          await quiz.save();
        }
      } else {
        const quiz = new QuizModel({
          category: category,
          difficulty:
            quizData.difficulty[0].toUpperCase() + quizData.difficulty.slice(1),
          questions: quizData.questions,
        });
        await quiz.save();
      }
    }

    console.log("Data import successful!");
  } catch (error) {
    console.error("Error importing data:", error.message);
  }
}

async function run(params) {
  try {
    await connectToDatabase();
    const apiData = await fetchDataFromApi(params);
    const organizedQuizzes = await organizeQuizzesByCategory(apiData);
    await importData(organizedQuizzes);
  } catch (error) {
    console.error("Error in the run function:", error.message);
  } finally {
    mongoose.connection.close();
    console.log("Disconnected from MongoDB");
  }
}

//array from 9 to 32
const array = Array.from({ length: 24 }, (_, i) => i + 9);

const difficulties = ["easy", "medium", "hard"];

async function runSequentially() {
  for (const number of array) {
    for (const difficulty of difficulties) {
      console.log(
        `Running for category ${number} and difficulty ${difficulty}`
      );
      await run(`&category=${number}&difficulty=${difficulty}`);
      await delay(5000);
    }
  }
}

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

runSequentially();
